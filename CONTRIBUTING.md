# Contributing Files to this Mirror

*Note: Any files contributed to this mirror must be redistributable. This means that their license must be in the REDISTRIBUTABLE license group, found in the `profiles/license_groups.yaml` file in the [openmw-mods repository](https://gitlab.com/portmod/openmw-mods/-/blob/master/profiles/license_groups.yaml) (or likely, the [meta repository](https://gitlab.com/portmod/meta/-/blob/master/profiles/license_groups.yaml)*

You will need git-lfs installed.

Start by forking this repository, and creating a new branch (named appropriately to the file to be added).

For each file to be added, you will need to run `git lfs track "$file"`. This will update the `.gitattributes`, which will need to be included when you commit the file.

After committing the files, create a merge request.

You may want to add or modify the following in section your `.gitconfig`, specifically noting the `--skip`. This prevents git lfs from downloading all the files in the mirror, allowing you to add files without having to download the entire multi-GB repository.

If you want to download files, they can be checked out with `git lfs fetch -I $file && git lfs checkout $file`.

```ini
[filter "lfs"]
	clean = git-lfs clean -- %f
	smudge = git-lfs smudge --skip %f
	process = git-lfs filter-process --skip
	required = true
```
